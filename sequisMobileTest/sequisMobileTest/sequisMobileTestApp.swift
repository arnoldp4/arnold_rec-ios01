//
//  sequisMobileTestApp.swift
//  sequisMobileTest
//
//  Created by Arnold Pangestu on 21/05/21.
//

import SwiftUI

@main
struct sequisMobileTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
