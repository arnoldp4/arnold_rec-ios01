//
//  Comment+CoreDataProperties.swift
//  sequisMobileTest
//
//  Created by Arnold Pangestu on 22/05/21.
//
//

import Foundation
import CoreData


extension Comment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Comment> {
        return NSFetchRequest<Comment>(entityName: "Comment")
    }

    @NSManaged public var id_author: String?
    @NSManaged public var name_comment: String?
    @NSManaged public var body_comment: String?
    @NSManaged public var date_comment: Date?

}

extension Comment : Identifiable {

}
