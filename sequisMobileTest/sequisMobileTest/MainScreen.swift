//
//  MainScreen.swift
//  sequisMobileTest
//
//  Created by Arnold Pangestu on 21/05/21.
//

import SwiftUI

struct MainScreen: View {
    @State var userDapat = [UserTest]()
    var body: some View {
        ScrollView {
            VStack(spacing: 10) {
                ForEach(userDapat) { list in
                    NavigationLink(destination: DetailScreen(gambar: list.download_url)) {
                        MainThumbnail(gambar: list.download_url, nama: list.author)
                    }
                }
            }.onAppear{
                ambilAPI().loadData { (ambil) in
                    self.userDapat = ambil
                }
            }
        }
    }
}

struct MainScreen_Previews: PreviewProvider {
    static var previews: some View {
        MainScreen()
    }
}
