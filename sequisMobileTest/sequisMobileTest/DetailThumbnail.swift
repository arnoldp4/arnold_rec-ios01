//
//  DetailThumbnail.swift
//  sequisMobileTest
//
//  Created by Arnold Pangestu on 22/05/21.
//

import SwiftUI

struct DetailThumbnail: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct DetailThumbnail_Previews: PreviewProvider {
    static var previews: some View {
        DetailThumbnail()
    }
}
