//
//  MainThumbnail.swift
//  sequisMobileTest
//
//  Created by Arnold Pangestu on 21/05/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct MainThumbnail: View {
    @State var gambar: String
    @State var nama: String
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 25, style: .continuous)
                .fill(Color.white)
                .frame(width: UIScreen.main.bounds.width/1.05, height: UIScreen.main.bounds.height/7)
                .shadow(radius: 5)
            HStack {
                WebImage(url: URL(string: gambar))
                    .resizable()
                    .scaledToFill()
                    .frame(width: UIScreen.main.bounds.width/2.6, height: UIScreen.main.bounds.height/7, alignment: .center)
                    .cornerRadius(25, corners: [.topLeft, .bottomLeft])
                VStack (alignment: .center) {
                    Text("Author:")
                        .bold()
                    Text(nama)
                        .bold()
                }
                .frame(width: UIScreen.main.bounds.width/1.8)
            }
        }
    }
}

extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
}

struct RoundedCorner: Shape {

    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}

struct MainThumbnail_Previews: PreviewProvider {
    static var previews: some View {
        MainThumbnail(gambar: "https://picsum.photos/id/0/5616/3744", nama: "Alejandro Escamilla")
    }
}
