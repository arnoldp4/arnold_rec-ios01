//
//  ContentView.swift
//  sequisMobileTest
//
//  Created by Arnold Pangestu on 21/05/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            MainScreen()
                .navigationBarTitle("Image List", displayMode: .inline)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
