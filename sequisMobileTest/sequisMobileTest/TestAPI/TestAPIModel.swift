//
//  TestAPIModel.swift
//  sequisMobileTest
//
//  Created by Arnold Pangestu on 21/05/21.
//

import SwiftUI

struct UserTest: Codable, Identifiable {
    let id = UUID()
    let author: String
    let download_url: String
}
