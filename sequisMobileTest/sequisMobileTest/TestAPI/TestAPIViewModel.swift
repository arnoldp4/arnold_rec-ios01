//
//  TestAPIViewModel.swift
//  sequisMobileTest
//
//  Created by Arnold Pangestu on 21/05/21.
//

import Foundation

class ambilAPI {
    func loadData(completion:@escaping ([UserTest]) -> ()) {
        guard let url = URL(string: "https://picsum.photos/v2/list") else { return }
        
        URLSession.shared.dataTask(with: url) { (data, _, _) in
            let users = try! JSONDecoder().decode([UserTest].self, from: data!)
            
            DispatchQueue.main.async {
                completion(users)
            }
        }.resume()
    }
}
