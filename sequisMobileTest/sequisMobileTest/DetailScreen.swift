//
//  DetailScreen.swift
//  sequisMobileTest
//
//  Created by Arnold Pangestu on 21/05/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct DetailScreen: View {
    @State var gambar: String
    var body: some View {
        VStack {
            WebImage(url: URL(string: gambar))
                .resizable()
                .scaledToFill()
        }
        .navigationBarTitle("Image Detail", displayMode: .inline)
    }
}

struct DetailScreen_Previews: PreviewProvider {
    static var previews: some View {
        DetailScreen(gambar: "https://picsum.photos/id/0/5616/3744")
    }
}
